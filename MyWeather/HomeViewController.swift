//
//  HomeViewController.swift
//  MyWeather
//
//  Created by Anatoliy on 11.11.2021.
//

import UIKit

class HomeViewController: UIViewController {
    
    lazy var swipe = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe))
    
    weak var delegate: HomeViewControllerDelegate?
    weak var closeDelegate: CloseMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        title = "Home"
        
        setupRecognizers()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "list.dash"),
                                                           style: .done, target: self,
                                                           action: #selector(barButtonTapped))
    }
    
    private func setupRecognizers() {
        //let swipe = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe))
        swipe.direction = .left
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didSwipe))
        
        view.addGestureRecognizer(swipe)
        view.addGestureRecognizer(tap)
    }
    
    
    @objc private func barButtonTapped() {
        delegate?.didTapMenuButton()
    }
    
    @objc private func didSwipe() {
        print("hi")
        closeDelegate?.closeMenu()
        
    }

}
