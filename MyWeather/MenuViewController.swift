//
//  MenuViewController.swift
//  MyWeather
//
//  Created by Anatoliy on 11.11.2021.
//

import UIKit

class MenuViewController: UIViewController {
    
    let cities = ["Анапа", "Краснодар", "Сочи", "Москва", "Пермь"]
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .none
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 50)
        return tableView
    }()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = CGRect(x: 0, y: view.safeAreaInsets.top, width: view.bounds.width * 0.5 - 10, height: view.bounds.height)
    }
    
}


extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    
        var content = cell.defaultContentConfiguration()
        
        content.text = "\(cities[indexPath.row])"
        content.textProperties.color = .systemBlue
        
        cell.contentConfiguration = content
        cell.backgroundColor = .none
        cell.contentView.backgroundColor = .none
        return cell
    }
    

    
    
}
