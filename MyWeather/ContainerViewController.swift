//
//  ContainerViewController.swift
//  MyWeather
//
//  Created by Anatoliy on 11.11.2021.
//

import UIKit

class ContainerViewController: UIViewController {

    
    
    enum MenuState {
        case opened, closed
    }
    
    private var menuState: MenuState = .closed
    
    let menuVC = MenuViewController()
    let homeVC = HomeViewController()
    var navVC: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addCildsVC()
    }
    
    private func addCildsVC() {
        
        addChild(menuVC)
        view.addSubview(menuVC.view)
        menuVC.didMove(toParent: self)
        
        homeVC.delegate = self
        homeVC.closeDelegate = self
        
        let navVC = UINavigationController(rootViewController: homeVC)
        self.navVC = navVC
        addChild(navVC)
        view.addSubview(navVC.view)
        navVC.didMove(toParent: self)
    }
    
    

    
}

extension ContainerViewController: HomeViewControllerDelegate {
    func didTapMenuButton() {
        
        switch menuState {
        case .opened:
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut) {
                self.navVC?.view.frame.origin.x = 0
            } completion: { [weak self] done in
                if done {
                    self?.menuState = .closed
                }
            }
            
        case .closed:
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut) {
                self.navVC?.view.frame.origin.x = self.homeVC.view.frame.width - self.homeVC.view.frame.width / 2
            } completion: { [weak self] done in
                if done {
                    self?.menuState = .opened
                }
            }
        }
    }
}

extension ContainerViewController: CloseMenuDelegate {
    func closeMenu() {
        
        switch menuState {
        case .opened:
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut) {
                self.navVC?.view.frame.origin.x = 0
            } completion: { [weak self] done in
                if done {
                    self?.menuState = .closed
                }
            }
            
        case .closed:
            break
        }
    }
}

