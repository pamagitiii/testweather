//
//  Protocols.swift
//  MyWeather
//
//  Created by Anatoliy on 11.11.2021.
//

import Foundation

protocol HomeViewControllerDelegate: AnyObject {
    func didTapMenuButton()
}

protocol CloseMenuDelegate: AnyObject {
    func closeMenu()
}
